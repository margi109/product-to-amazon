<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Website</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Raleway:300,300i,400,400i,600,600i,700,700i,900" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="assets/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="assets/vendor/icofont/icofont.min.css" rel="stylesheet">
  <link href="assets/vendor/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="assets/vendor/animate.css/animate.min.css" rel="stylesheet">
  <link href="assets/vendor/venobox/venobox.css" rel="stylesheet">
  <link href="assets/vendor/owl.carousel/assets/owl.carousel.min.css" rel="stylesheet">
  <link href="assets/vendor/aos/aos.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: Mamba - v2.5.1
  * Template URL: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Top Bar ======= -->
  <!-- <section id="topbar" class="d-none d-lg-block">
    <div class="container clearfix">
      <div class="contact-info float-left">
        <i class="icofont-envelope"></i><a href="mailto:contact@example.com">contact@example.com</a>
        <i class="icofont-phone"></i> +1 5589 55488 55
      </div>
      <div class="social-links float-right">
        <a href="#" class="twitter"><i class="icofont-twitter"></i></a>
        <a href="#" class="facebook"><i class="icofont-facebook"></i></a>
        <a href="#" class="instagram"><i class="icofont-instagram"></i></a>
        <a href="#" class="skype"><i class="icofont-skype"></i></a>
        <a href="#" class="linkedin"><i class="icofont-linkedin"></i></i></a>
      </div>
    </div>
  </section> -->

  <!-- ======= Header ======= -->
  <header id="header">
    <div class="container">

      <div class="logo float-left">
        <h1 class="text-light"><a href="index.html"><span>Products</span></a></h1>
        <!-- Uncomment below if you prefer to use an image logo -->
        <!-- <a href="index.html"><img src="assets/img/logo.png" alt="" class="img-fluid"></a>-->
      </div>

     
    </div>
  </header><!-- End Header -->


  <main id="main">

    
    <!-- ======= About Lists Section ======= -->
    <section class="about-lists">
      <div class="container">

        <div class="row no-gutters">

          <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up">
            <span><img src="assets/img/portfolio/portfolio-1.jpg" class="img-fluid" alt=""></span>
            <h4>Headphone</h4>
            <p>OneOdio Over Ear Headphone, Wired Bass Headsets with 50mm Driver, Foldable Lightweight Headphones with Shareport and Mic for Recording Monitoring Podcast Guitar PC TV - (PRO-10 Red)</p>
            <div class="text-center mt-10">
            <a target="_blank"  class="btn btn-primary" href="https://www.amazon.in/s?k=headphone&ref=nb_sb_noss_2">Buy now</a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up" data-aos-delay="100">
            <span><img src="assets/img/portfolio/portfolio-2.jpg" class="img-fluid" alt=""></span>
            <h4>Watch</h4>
            <p>Analogue Men's Grey Dial Watch - 11S9841L01 (Dark black Strap)</p>
            <div class="text-center mt-10">
            <a target="_blank" href="https://www.amazon.in/s?k=watch&ref=nb_sb_noss_1" class="btn btn-primary">Buy now</a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up" data-aos-delay="200">
            <span><img src="assets/img/portfolio/portfolio-3.jpg" class="img-fluid" alt=""></span>
            <h4>Sunglass</h4>
            <p>Unisex Adult Round Sunglasses</p>
            <div class="text-center mt-10">
            <a target="_blank" href="https://www.amazon.in/s?k=sunglasses&ref=nb_sb_noss_2" class="btn btn-primary">Buy now</a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up" data-aos-delay="300">
            <span><img src="assets/img/portfolio/portfolio-4.jpg" class="img-fluid" alt=""></span>
            <h4>Cactus</h4>
            <p>HILLMART Cute NOTO Cactus roseoluteus Ball Cactus with Little Artistic Pot- 5.5 cm</p>
            <div class="text-center mt-10">
            <a target="_blank" href="https://www.amazon.in/s?k=cactus&crid=1U1UKABCDB29R&sprefix=cacatus%2Caps%2C467&ref=nb_sb_noss_1" class="btn btn-primary">Buy now</a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up" data-aos-delay="400">
            <span><img src="assets/img/portfolio/portfolio-5.jpg" class="img-fluid" alt=""></span>
            <h4>Perfume</h4>
            <p>Ustraa Base Camp Cologne - 100 ml - Perfume</p>
            <div class="text-center mt-10">
            <a target="_blank" href="https://www.amazon.in/s?k=perfume&ref=nb_sb_noss_1" class="btn btn-primary">Buy now</a>
            </div>
          </div>

          <div class="col-lg-4 col-md-6 content-item" data-aos="fade-up" data-aos-delay="500">
            <span><img src="assets/img/portfolio/portfolio-6.jpg" class="img-fluid" alt=""></span>
            <h4>Coconut oil</h4>
            <p> Skin Nourishing Oil, For Baby Massage, For Skin Massage, 150 ml</p>
            <div class="text-center mt-10">
            <a target="_blank" class="btn btn-primary" href="https://www.amazon.in/s?k=coconut+oil&ref=nb_sb_noss_2">Buy now</a>
            </div>
          </div>

        </div>

      </div>
    </section><!-- End About Lists Section -->

   
    
    
   
 
  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer">
    

    <div class="container">
      <div class="copyright">
        &copy; Copyright <strong><span>Products</span></strong>. All Rights Reserved
      </div>
      <div class="credits">
        <!-- All the links in the footer should remain intact. -->
        <!-- You can delete the links only if you purchased the pro version. -->
        <!-- Licensing information: https://bootstrapmade.com/license/ -->
        <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/mamba-one-page-bootstrap-template-free/ -->
        Designed by <a href="#">Margi Varmora</a>
      </div>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top"><i class="icofont-simple-up"></i></a>

  <!-- Vendor JS Files -->
  <script src="assets/vendor/jquery/jquery.min.js"></script>
  <script src="assets/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="assets/vendor/jquery.easing/jquery.easing.min.js"></script>
  <script src="assets/vendor/php-email-form/validate.js"></script>
  <script src="assets/vendor/jquery-sticky/jquery.sticky.js"></script>
  <script src="assets/vendor/venobox/venobox.min.js"></script>
  <script src="assets/vendor/waypoints/jquery.waypoints.min.js"></script>
  <script src="assets/vendor/counterup/counterup.min.js"></script>
  <script src="assets/vendor/owl.carousel/owl.carousel.min.js"></script>
  <script src="assets/vendor/isotope-layout/isotope.pkgd.min.js"></script>
  <script src="assets/vendor/aos/aos.js"></script>

  <!-- Template Main JS File -->
  <script src="assets/js/main.js"></script>

</body>

</html>